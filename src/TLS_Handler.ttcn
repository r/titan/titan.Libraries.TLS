///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2019 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:               TLS_Handler.ttcn
//  Description:        Type and function definition for TLS
//  Rev:                R3A
//  Prodnr:             CNL 113 839
//

module TLS_Handler {

type enumerated TLS_op_result{TLS_OK, TLS_NEED_MORE_DATA,TLS_DATA_TO_SEND,TLS_ERROR}

// Creates a new TLS/SSL object. The created object should be suplied to the 
// handshake function.
external function TLS_New_object(in TLS_descriptor descr, // The parameters of the connection
                                 out integer object_id,   // The identifier of the created TLS object
                                 in integer user_idx := -1  // Transparent user data. Can be retrieved with TLS_get_user_idx() function
                                                            // A lower layer connection index can be stored here
                                                            // For example, the IPL4 connId of the connection used to send and receive data
                                                            // Or the index of the database which holds the user data can be stored here
                                 ) return TLS_op_result;

type enumerated TLS_method { TLS_method_TLS, TLS_method_DTLS }

type enumerated TLS_Supported_proto_versions { TLS_SSL3_VERSION, TLS_TLS1_VERSION, TLS_TLS1_1_VERSION, TLS_TLS1_2_VERSION , TLS_DTLS1_VERSION, TLS_DTLS1_2_VERSION}

// Parameters of the TLS 
type record TLS_descriptor {
  TLS_method  tls_method,  // Which method to use: TLS/SSL or DTLS?
  TLS_Supported_proto_versions min_supported_version optional,  // Minimum version of the supported protocol
  TLS_Supported_proto_versions max_supported_version optional,  // Maximum version of the supported protocol
  charstring  ssl_key_file optional,
  charstring  ssl_certificate_file optional,
  charstring  ssl_trustedCAlist_file optional,
  charstring  ssl_cipher_list optional,
  charstring  ssl_password optional,  // private key file password
  boolean     ssl_verify_certificate optional,
  charstring  psk_hint optional,
  charstring  psk_identity optional,
  charstring  psk_key optional,
  boolean     psk_for_server optional
}

// Dispose the TLS/SSL object.
external function TLS_Delete_object(in integer object_id) return TLS_op_result;

// Get or set the transparent user_idx
external function TLS_get_user_idx(in integer object_id, out integer user_idx) return TLS_op_result;
external function TLS_set_user_idx(in integer object_id, in integer user_idx) return TLS_op_result;

// Handshake function.
// Should be called repetadly until the handshake is finished.
// Input stream: The data received from peer
// Output stream: The data should be sent ot the peer
//
// return values:
//    TLS_OK:  handshake finshed
//    TLS_NEED_MORE_DATA: send the output stream to the peer if len >0,
//                        call the function again with more data from peer
//    TLS_DATA_TO_SEND: send the output stream to the peer, 
//                      and call the function again
//    TLS_ERROR:  soemthing went wrong.
external function TLS_Handshake(in integer object_id, // The object_id, created by the TLS_New_object
                                in boolean is_server, // True: server side, false: client side
                                in octetstring input_stream, // The input stream, data received from peer
                                out octetstring output_stream // the output stream. Send this to the peer if needed
                                ) return TLS_op_result;


// Encrypt data to send to the peer
// Should be called repetadly until returns TLS_OK.
// User data: The user data to send
// Input stream: The data received from peer
// Output stream: The data should be sent ot the peer
//
// return values:
//    TLS_OK: All of the user data is encrypted and ready to send. Send the output_stream to peer
//    TLS_NEED_MORE_DATA: send the output stream to the peer if len >0,
//                        call the function again with data from peer
//    TLS_DATA_TO_SEND: send the output stream to the peer, and call the function again.
//    TLS_ERROR:  soemthing went wrong.
//
// If the function should be called again, call it with the same user_data as used in the first time
external function TLS_Write(in integer object_id, // The object_id, created by the TLS_New_object
                                in octetstring user_data, // The user data to be encrypted.
                                in octetstring input_stream, // The input stream, data received from peer
                                out octetstring output_stream // the output stream. Send this to the peer if needed
                                ) return TLS_op_result;


// Decrypt received data to from the peer
// Should be called repetadly until returns TLS_NEED_MORE_DATA.
// The function return TLS_OK if it succefuly decrypted a TLS record,
// but the function should be called again to make sure all of the eceived
// TLS record are processed.
// User data: The user decypted user data
// Input stream: The data received from peer
// Output stream: The data should be sent ot the peer
//
// return values:
//    TLS_OK: All of the user data is decrypted and ready to be used.
//    TLS_NEED_MORE_DATA: send the output stream to the peer if len >0,
//                        call the function again with data from peer
//    TLS_DATA_TO_SEND: send the output stream to the peer, and call the function again.
//    TLS_ERROR:  soemthing went wrong.
//

external function TLS_Read(in integer object_id, // The object_id, created by the TLS_New_object
                                out octetstring user_data, // The user data to be encrypted.
                                in octetstring input_stream, // The input stream, data received from peer
                                out octetstring output_stream // the output stream. Send this to the peer if needed
                                ) return TLS_op_result;


}
